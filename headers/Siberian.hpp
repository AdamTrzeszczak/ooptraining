/*
 * Siberian.h
 *
 *  Created on: Mar 20, 2019
 *      Author: atrzeszczak
 */

#ifndef HEADERS_SIBERIAN_HPP_
#define HEADERS_SIBERIAN_HPP_

#include "CatBase.hpp"

class Siberian : public CatBase
{
public:
	Siberian();
	~Siberian();
};

#endif /* HEADERS_SIBERIAN_HPP_ */
