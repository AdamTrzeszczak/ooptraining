/*
 * Triangle.hpp
 *
 *  Created on: Mar 19, 2019
 *      Author: atrzeszczak
 */

#ifndef HEADERS_TRIANGLE_HPP_
#define HEADERS_TRIANGLE_HPP_

#include <Shape.hpp>

class Triangle : public Shape
{
public:
	Triangle(float pWidth, float pHeight) :
			width(pWidth), height(pHeight)
	{
	}

	~Triangle() = default;

	float area() const override;

private:
	float width;
	float height;
};

#endif /* HEADERS_TRIANGLE_HPP_ */
