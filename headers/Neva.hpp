/*
 * Siberian.h
 *
 *  Created on: Mar 20, 2019
 *      Author: atrzeszczak
 */

#ifndef HEADERS_NEVA_HPP_
#define HEADERS_NEVA_HPP_

#include "CatBase.hpp"

class Neva : public CatBase
{
public:
	Neva();
	~Neva();
};

#endif /* HEADERS_NEVA_HPP_ */
