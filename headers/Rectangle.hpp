/*
 * Rectangle.hpp
 *
 *  Created on: Mar 18, 2019
 *      Author: atrzeszczak
 */

#ifndef HEADERS_RECTANGLE_HPP_
#define HEADERS_RECTANGLE_HPP_

#include <Shape.hpp>

class Rectangle : public Shape
{
public:
	Rectangle(float pSide1, float pSide2) :
			side1(pSide1), side2(pSide2)
	{
	}

	~Rectangle() = default;

	float area() const override;

private:
	float side1;
	float side2;
};


#endif /* HEADERS_RECTANGLE_HPP_ */
