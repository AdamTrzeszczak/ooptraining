/*
 * TaskLauncher.h
 *
 *  Created on: Mar 19, 2019
 *      Author: atrzeszczak
 */

#ifndef HEADERS_TASKLAUNCHER_H_
#define HEADERS_TASKLAUNCHER_H_

class TaskLauncher
{
public:
	virtual ~TaskLauncher() = default;
	virtual void launchTask() = 0;
};

#endif /* HEADERS_TASKLAUNCHER_H_ */
