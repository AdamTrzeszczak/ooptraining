/*
 * Fighter.h
 *
 *  Created on: Mar 18, 2019
 *      Author: atrzeszczak
 */

#ifndef HEADERS_FIGHTER_H_
#define HEADERS_FIGHTER_H_

#include <iostream>
#include <string>

class Fighter
{
public:
	Fighter(uint8_t pDamageAttack, uint8_t pHealth, std::string pName) :
			name(pName), health(pHealth), damageAttack(pDamageAttack)
	{
	}

	uint8_t getDamageAttack() const;
	
	void setDamageAttack(uint8_t damageAttack);
	
	uint8_t getHealth() const;
	
	void setHealth(uint8_t health);
	
	const std::string& getName() const;
	
	void setName(const std::string& name);
	
	std::string toString();
	
	static const std::string& startFight(Fighter& fighter1, Fighter& fighter2);

private:
	std::string name;
	uint8_t health;
	uint8_t damageAttack;
};

#endif /* HEADERS_FIGHTER_H_ */
