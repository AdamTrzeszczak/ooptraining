/*
 * Shape.hpp
 *
 *  Created on: Mar 19, 2019
 *      Author: atrzeszczak
 */

#ifndef HEADERS_SHAPE_HPP_
#define HEADERS_SHAPE_HPP_

class Shape
{
public:
	virtual ~Shape() = default;
	virtual float area() const = 0;
};

#endif /* HEADERS_SHAPE_HPP_ */
