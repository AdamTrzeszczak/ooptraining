/*
 * Cat.hpp
 *
 *  Created on: Mar 18, 2019
 *      Author: atrzeszczak
 */

#ifndef CAT_HPP_
#define CAT_HPP_

#include <string>

class Cat
{
public:
	Cat()
	{
		weight = 5;
	}
	Cat(uint8_t pWeigt, std::string pName) :
			weight(pWeigt), name(pName)
	{
	}

	void meow(uint8_t howLoud);
	void setName(std::string newName);
	std::string getName();

private:
	uint8_t weight;
	std::string name;
};

#endif /* CAT_HPP_ */
