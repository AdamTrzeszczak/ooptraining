/*
 * ICat.hpp
 *
 *  Created on: Mar 20, 2019
 *      Author: atrzeszczak
 */

#ifndef HEADERS_CATBASE_HPP_
#define HEADERS_CATBASE_HPP_

#include <iostream>

class CatBase
{
public:

	virtual ~CatBase()
	{
		std::cout << "ICat ~dtor()." << std::endl;
	}
};



#endif /* HEADERS_CATBASE_HPP_ */
