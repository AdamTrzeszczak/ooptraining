/*
 * ComplexNumber.hpp
 *
 *  Created on: Mar 18, 2019
 *      Author: atrzeszczak
 */

#ifndef HEADERS_COMPLEXNUMBER_HPP_
#define HEADERS_COMPLEXNUMBER_HPP_

#include <string>

class ComplexNumber
{
public:
	ComplexNumber(int pRealPart, int pImaginaryPart) :
			realPart(pRealPart), imaginaryPart(pImaginaryPart)
	{
	}

	ComplexNumber operator+(const ComplexNumber& other) const;

	std::string toString();
private:
	int realPart;
	int imaginaryPart;
};

#endif /* HEADERS_COMPLEXNUMBER_HPP_ */
