/*
 * CatLauncher.hpp
 *
 *  Created on: Mar 19, 2019
 *      Author: atrzeszczak
 */

#ifndef HEADERS_LAUNCHERS_CATINHERITANCELAUNCHER_HPP_
#define HEADERS_LAUNCHERS_CATINHERITANCELAUNCHER_HPP_

#include <TaskLauncher.h>

class CatInheritanceLauncher : public TaskLauncher
{
public:
	~CatInheritanceLauncher() = default;
	void launchTask() override;
};

#endif /* HEADERS_LAUNCHERS_CATINHERITANCELAUNCHER_HPP_ */
