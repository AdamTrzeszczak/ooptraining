/*
 * NumbersLauncher.h
 *
 *  Created on: Mar 19, 2019
 *      Author: atrzeszczak
 */

#ifndef HEADERS_LAUNCHERS_NUMBERSLAUNCHER_H_
#define HEADERS_LAUNCHERS_NUMBERSLAUNCHER_H_

#include <TaskLauncher.h>

class NumbersLauncher : public TaskLauncher
{
public:
	~NumbersLauncher() = default;
	void launchTask() override;

};

#endif /* HEADERS_LAUNCHERS_NUMBERSLAUNCHER_H_ */
