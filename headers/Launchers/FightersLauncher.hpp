/*
 * FightersLauncher.hpp
 *
 *  Created on: Mar 19, 2019
 *      Author: atrzeszczak
 */

#ifndef HEADERS_LAUNCHERS_FIGHTERSLAUNCHER_HPP_
#define HEADERS_LAUNCHERS_FIGHTERSLAUNCHER_HPP_

#include <TaskLauncher.h>

class FightersLauncher : public TaskLauncher
{
public:
	~FightersLauncher() = default;
	void launchTask() override;
};

#endif /* HEADERS_LAUNCHERS_FIGHTERSLAUNCHER_HPP_ */
