/*
 * ShapesLauncher.hpp
 *
 *  Created on: Mar 19, 2019
 *      Author: atrzeszczak
 */

#ifndef HEADERS_LAUNCHERS_SHAPESLAUNCHER_HPP_
#define HEADERS_LAUNCHERS_SHAPESLAUNCHER_HPP_

#include <TaskLauncher.h>

class ShapesLauncher : public TaskLauncher
{
public:
	~ShapesLauncher() = default;
	void launchTask() override;
};

#endif /* HEADERS_LAUNCHERS_SHAPESLAUNCHER_HPP_ */
