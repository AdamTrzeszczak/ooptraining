/*
 * CatLauncher.hpp
 *
 *  Created on: Mar 19, 2019
 *      Author: atrzeszczak
 */

#ifndef HEADERS_LAUNCHERS_CATLAUNCHER_HPP_
#define HEADERS_LAUNCHERS_CATLAUNCHER_HPP_

#include <TaskLauncher.h>

class CatLauncher : public TaskLauncher
{
public:
	~CatLauncher() = default;
	void launchTask() override;
};

#endif /* HEADERS_LAUNCHERS_CATLAUNCHER_HPP_ */
