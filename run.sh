#!/usr/bin/env bash

cd build/
rm -rf ./*
cmake ..
make
./start_app
cd ../
