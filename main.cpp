/*
 * main.cpp
 *
 *  Created on: Mar 18, 2019
 *      Author: atrzeszczak
 */

#include <iostream>
#include <vector>
#include <memory>

#include <Launchers/NumbersLauncher.h>
#include <Launchers/CatLauncher.hpp>
#include <Launchers/FightersLauncher.hpp>
#include <TaskLauncher.h>

int main()
{
	std::vector<std::shared_ptr<TaskLauncher>> launchers;
	launchers.push_back(std::make_shared<NumbersLauncher>());
	launchers.push_back(std::make_shared<CatLauncher>());
	launchers.push_back(std::make_shared<FightersLauncher>());

	for (auto& launcher : launchers)
	{
		launcher->launchTask();
		std::cout << "=============================" << std::endl;
	}

	return 0;
}

