/*
 * FightersLauncher.cpp
 *
 *  Created on: Mar 19, 2019
 *      Author: atrzeszczak
 */

#include <iostream>

#include <Launchers/FightersLauncher.hpp>
#include <Fighter.h>

void FightersLauncher::launchTask()
{
	std::cout << "Hello world, let's start a fight!" << std::endl;

	Fighter fighter1(10, 234, "Nelson");
	Fighter fighter2(20, 174, "O'Neil");

	std::cout << "In the first corner there is " << fighter1.toString() << std::endl;
	std::cout << "In the second corner there is " << fighter2.toString() << std::endl;

	std::cout << "Let the fight begin!" << std::endl;
	std::cout << "And the winner is:" << Fighter::startFight(fighter1, fighter2) << std::endl;
}
