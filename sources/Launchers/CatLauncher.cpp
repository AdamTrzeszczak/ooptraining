/*
 * CatLauncher.cpp
 *
 *  Created on: Mar 19, 2019
 *      Author: atrzeszczak
 */

#include <iostream>
#include <random>

#include <Launchers/CatLauncher.hpp>
#include <Cat.hpp>

void CatLauncher::launchTask()
{
	std::cout << "Hello cat!" << std::endl;

	std::random_device randomDevice;
	std::mt19937 generator(randomDevice());
	std::uniform_int_distribution<uint8_t> distribution(1, 20);
	uint8_t catMeowStrength = distribution(generator);

	Cat cat(6, "Nico");

	std::cout << "Cat " << cat.getName() << ", greets you with random loudness!" << std::endl;
	cat.meow(catMeowStrength);
}
