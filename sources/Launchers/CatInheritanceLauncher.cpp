/*
 * CatLauncher.cpp
 *
 *  Created on: Mar 19, 2019
 *      Author: atrzeszczak
 */

#include <iostream>
#include <random>

#include <Launchers/CatInheritanceLauncher.hpp>
#include <Neva.hpp>
#include <Siberian.hpp>
#include "../../headers/CatBase.hpp"

void CatInheritanceLauncher::launchTask()
{
	Neva nevaCat;
	Siberian siberianCat;

	CatBase* catPointer = new Neva();

	delete catPointer;

	catPointer = new Siberian();

	delete catPointer;

	catPointer = &nevaCat;
}
