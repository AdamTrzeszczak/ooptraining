/*
 * NumbersLauncher.cpp
 *
 *  Created on: Mar 19, 2019
 *      Author: atrzeszczak
 */

#include <iostream>

#include <Launchers/NumbersLauncher.h>

#include <Rectangle.hpp>
#include <ComplexNumber.hpp>

void NumbersLauncher::launchTask()
{
	std::cout << "Let's calculate rectange area!" << std::endl;

	float side1, side2;

	std::cout << "Input first side: ";
	std::cin >> side1;

	std::cout << "Input second side: ";
	std::cin >> side2;

	Rectangle rectangle(side1, side2);

	std::cout << "Your rectangle area equals: " << rectangle.area() << std::endl;

	std::cout << "Now let's sum two complex numbers!" << std::endl;

	float realPart, imaginaryPart;

	std::cout << "Input first number real part: ";
	std::cin >> realPart;

	std::cout << "Input first number imaginary part: ";
	std::cin >> imaginaryPart;

	ComplexNumber complexNumber1(realPart, imaginaryPart);

	std::cout << "Input second number real part: ";
	std::cin >> realPart;

	std::cout << "Input second number imaginary part: ";
	std::cin >> imaginaryPart;

	ComplexNumber complexNumber2(realPart, imaginaryPart);

	std::cout << complexNumber1.toString() + " + " + complexNumber2.toString() + "=" + (complexNumber1 + complexNumber2).toString() << std::endl;
}
