/*
 * ShapesLauncher.cpp
 *
 *  Created on: Mar 19, 2019
 *      Author: atrzeszczak
 */
#include <iostream>

#include <Launchers/ShapesLauncher.hpp>
#include <Shape.hpp>
#include <Triangle.hpp>
#include <Rectangle.hpp>

void ShapesLauncher::launchTask()
{
	Triangle triangle(4.5, 5.3);
	Rectangle rectangle(8.3, 2.7);

	std::cout << "Triangle area = " << triangle.area() << std::endl;
	std::cout << "Rectangle area = " << rectangle.area() << std::endl;

	Shape* shapePtr = &triangle;

	std::cout << "Shape = Triangle" << std::endl;

	std::cout << "Shape area = " << shapePtr->area() << std::endl;

	shapePtr = &rectangle;

	std::cout << "Shape = Rectangle" << std::endl;

	std::cout << "Shape area = " << shapePtr->area() << std::endl;
}
