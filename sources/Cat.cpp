/*
 * Cat.cpp
 *
 *  Created on: Mar 18, 2019
 *      Author: atrzeszczak
 */
#include <iostream>

#include "../headers/Cat.hpp"

void Cat::meow(uint8_t howLoud)
{
	std::string exclamation;
	exclamation.append(howLoud, '!');
	std::cout << "meow" << exclamation << std::endl;
}

void Cat::setName(std::string newName)
{
	name = newName;
}

std::string Cat::getName()
{
	return name;
}
