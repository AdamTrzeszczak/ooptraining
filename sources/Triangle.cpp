/*
 * Triangle.cpp
 *
 *  Created on: Mar 19, 2019
 *      Author: atrzeszczak
 */

#include <Triangle.hpp>

float Triangle::area() const
{
	return (width * height) / 2.0;
}
