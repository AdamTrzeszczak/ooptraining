/*
 * Fighter.cpp
 *
 *  Created on: Mar 18, 2019
 *      Author: atrzeszczak
 */

#include <sstream>

#include "../headers/Fighter.h"

uint8_t Fighter::getDamageAttack() const
{
	return damageAttack;
}

void Fighter::setDamageAttack(uint8_t damageAttack)
{
	this->damageAttack = damageAttack;
}

uint8_t Fighter::getHealth() const
{
	return health;
}

void Fighter::setHealth(uint8_t health)
{
	this->health = health;
}

const std::string& Fighter::getName() const
{
	return name;
}

void Fighter::setName(const std::string& name)
{
	this->name = name;
}

std::string Fighter::toString()
{
	std::ostringstream stringStream;
	stringStream << "Fighter " << name << ", health:" << static_cast<int>(health) << ", power:" << static_cast<int>(damageAttack);
	return stringStream.str();
}

const std::string& Fighter::startFight(Fighter& fighter1, Fighter& fighter2)
{
	if ((fighter1.damageAttack == 0) || (fighter2.damageAttack == 0))
	{
		return "One of the fighters is injured and deals no damage!";
	}

	while ((fighter1.damageAttack < fighter2.health) && (fighter2.damageAttack < fighter1.health))
	{
		fighter1.health -= fighter2.damageAttack;
		fighter2.health -= fighter1.damageAttack;
		
		std::cout << fighter1.name << " takes " << static_cast<int>(fighter2.damageAttack) << " damage!" << std::endl;
		std::cout << fighter2.name << " takes " << static_cast<int>(fighter1.damageAttack) << " damage!" << std::endl;
	}

	if (fighter1.damageAttack >= fighter2.health)
	{
		return fighter1.name;
	}
	else if (fighter2.damageAttack >= fighter1.health)
	{
		return fighter2.name;
	}
	else
	{
		return "Something went really bad!";
	}
}
