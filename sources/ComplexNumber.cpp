/*
 * ComplexNumber.cpp
 *
 *  Created on: Mar 18, 2019
 *      Author: atrzeszczak
 */

#include <sstream>

#include <ComplexNumber.hpp>

ComplexNumber ComplexNumber::operator +(
		const ComplexNumber& other) const
{
	return ComplexNumber(realPart + other.realPart,
			imaginaryPart + other.imaginaryPart);
}

std::string ComplexNumber::toString()
{
	std::ostringstream stringStream;
	stringStream << "(" << realPart << (imaginaryPart > 0 ? ("+") : "") << imaginaryPart << "i)";
	return stringStream.str();
}
