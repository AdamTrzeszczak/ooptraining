/*
 * Siberian.cpp
 *
 *  Created on: Mar 20, 2019
 *      Author: atrzeszczak
 */

#include <iostream>
#include <Siberian.hpp>

Siberian::Siberian()
{
	std::cout << "Siberian cat ctor()." << std::endl;
}

Siberian::~Siberian()
{
	std::cout << "Siberian cat ~dtor()." << std::endl;
}

