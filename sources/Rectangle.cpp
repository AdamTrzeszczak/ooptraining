/*
 * Rectangle.cpp
 *
 *  Created on: Mar 18, 2019
 *      Author: atrzeszczak
 */

#include <Rectangle.hpp>

float Rectangle::area() const
{
	return side1 * side2;
}
