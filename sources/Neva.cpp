/*
 * Siberian.cpp
 *
 *  Created on: Mar 20, 2019
 *      Author: atrzeszczak
 */

#include <iostream>
#include <Neva.hpp>

Neva::Neva()
{
	std::cout << "Neva cat ctor()." << std::endl;
}

Neva::~Neva()
{
	std::cout << "Neva cat ~dtor()." << std::endl;
}

